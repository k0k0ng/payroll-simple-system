<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php 
		if (!isset($_SESSION['username'])) {
			header('location:Login_page.php');
		} else {
			header('location:Profile_page.php');
		}
	?>
</body>
</html>