<?php 
  session_start();
  include_once 'backend/DB.php';

  if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
    $sql = "SELECT * FROM accounts WHERE username='$username';"; 
    $result = mysqli_query($conn, $sql);
    $resultCheck = mysqli_num_rows($result);
    $rows = mysqli_fetch_assoc($result); 

    

  } else {
    header('location:Login_page.php?invalid-action');
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="shortcut icon" type="image/png" href="images/sad.png">

    <title>Payroll System</title>

    <!-- Bootstrap core CSS -->
    <link href="./bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./bootstrap/dashboard.css" rel="stylesheet">

    <style type="text/css">
      .tap{
        position: absolute;
        top: 95%;
      }
    </style>
 
</head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
        Payroll System
      </a>
      <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="backend/log-out.php">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">

              <li class="nav-item">
                <a class="nav-link active" href="Admin_dashboard.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="Applicant_list.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                  Applicants
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="Employee_page.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  Employee
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="Client_company.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart-2"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
                  Client Company
                </a>
              </li>
              
            </ul>

          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
        
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h5">Add new applicant</h1>
          </div>

          <div class="container">

            <h4>Applicant Information</h4><br>

          
              <div class="row justify-content-center">
                <div class="col-sm-12 ">
                  <form action="backend/Insert_applicant.php" method="POST" class="form-horizontal">

                    <div class="col-sm-3 offset-md-2"> 
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">ID</span>
                        </div>
                        <input name="id" type="text" aria-label="id" class="form-control is-invalid" required>
                         <div class="invalid-feedback">
                            Invalid ID number! Please make sure that the ID number is not yet taken.
                          </div>
                      </div>
                    </div> <br>

                    <div class="form-group row">
                      <label for="fname" class="col-sm-1 offset-md-1 control-label">Firstname</label>
                      <div class="col-sm-7">
                         <input type="text" class="form-control" id="fname" name="fname" required/>
                       </div>
                    </div>

                    <div class="form-group row">
                      <label for="mname" class="col-sm-1 offset-md-1 control-label">Middlename</label>
                      <div class="col-sm-7">
                         <input type="text" class="form-control" id="mname" name="mname" />
                       </div>
                    </div>

                    <div class="form-group row">
                      <label for="lname" class="col-sm-1 offset-md-1 control-label">Lastname</label>
                      <div class="col-sm-7">
                         <input type="text" class="form-control" id="lname" name="lname" required/>
                       </div>
                    </div><br>

                    <div class="col-sm-3 offset-md-2">
                      <div class="input-group ">
                        <div class="input-group-prepend">
                          <label class="input-group-text" for="gender">Gender</label>
                        </div>
                        <select class="custom-select" id="gender" name="gender">
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                          <option value="Either">Either</option>
                        </select>
                      </div>
                    </div><br>

                    <div class="col-sm-6 offset-md-2"> 
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Birth date</span>
                        </div>
                        
                        <?php
                          for ($x = 1; $x <= 31; $x++){
                            $Days[] = $x;
                          }
                          echo '<select name="day" class="custom-select">';
                          foreach ($Days as $option){
                            echo '<option value="'.$option.'">'.$option.'</option>';
                          }
                          echo '</select>';
                        ?>
                        <select name="month" class="custom-select">
                          <option value="01">Jan</option>
                          <option value="02">Feb</option>
                          <option value="03">Mar</option>
                          <option value="04">Apr</option>
                          <option value="05">May</option>
                          <option value="06">Jun</option>
                          <option value="07">Jul</option>
                          <option value="08">Aug</option>
                          <option value="09">Sep</option>
                          <option value="10">Oct</option>
                          <option value="11">Nov</option>
                          <option value="12">Dec</option>
                        </select>
                        <?php
                          $currentYear = date("Y");
                          for ($x = $currentYear; $x >= 1950; $x--){
                            $Years[] = $x;
                          }
                          echo '<select name="year" class="custom-select">';
                          foreach ($Years as $option){
                            echo '<option value="'.$option.'">'.$option.'</option>';
                          }
                          echo '</select>';
                        ?>
                      </div>

                    </div> <br>

                    <div class="col-sm-3 offset-md-2">
                        
                      <div class="input-group ">
                        <div class="input-group-prepend">
                          <label class="input-group-text" for="civil_stat">Civil Status</label>
                        </div>
                        <select class="custom-select" id="civil_stat" name="civil_stat">
                          <option value="Single">Single</option>
                          <option value="Maried">Maried</option>
                          <option value="Widowed">Widowed</option>
                          <option value="Separated">Separated</option>
                          <option value="Divorced">Divorced</option>
                        </select>
                      </div>
                    </div> <br>

                    <div class="form-group row">
                      <label for="email" class="col-sm-1 offset-md-1 control-label">Address</label>
                        <div class="col-sm-7">
                          <div class="input-group">
                            <input name="address" type="text" class="form-control" >
                          </div>
                         </div> <br><br><br>
                    </div>

                    <div class="col-sm-3 offset-md-2">
                        
                      <div class="input-group ">
                        <div class="input-group-prepend">
                          <label class="input-group-text" for="religion">Religion</label>
                        </div>
                        <select class="custom-select" id="religion" name="religion">
                          <option value="Christianity">Christianity</option>
                          <option value="Islam">Islam</option>
                          <option value="Mormon">Mormon</option>
                          <option value="7th Day Adventist">7th Day Adventist</option>
                          <option value="Atheist">Atheist</option>
                        </select>
                      </div>
                    </div> <br><br>

                    <div class="form-group row">
                      <label for="cell_num" class="col-sm-1 offset-md-1 control-label">Cell number:</label>
                      <div class="col-sm-7">
                         <input type="cell_num" class="form-control" id="cell_num" name="cell_num" required/>
                       </div>
                    </div>

                    <div class="form-group row">
                      <label for="email" class="col-sm-1 offset-md-1 control-label">Email</label>
                      <div class="col-sm-7">
                         <input type="email" class="form-control" id="email" name="email" required/>
                       </div>
                    </div>
                    
                    <div class="form-group row">
                      <label for="tel_num" class="col-sm-1 offset-md-1 control-label">Tel number:</label>
                      <div class="col-sm-7">
                         <input type="tel_num" class="form-control" id="tel_num" name="tel_num"/>
                       </div>
                    </div>
                    <br><br><br>
                    <button type="submit" class="btn btn-outline-primary offset-md-4 btn-lg" name="add">Add applicant</button>

                  </form>
                  
                  <form action="Admin_dashboard.php">
                    <button type="submit" class="btn btn-outline-dark tap offset-md-7" name="cancel">Cancel</button>
                  </form>
                </div>
              </div>
              <br><br><br><br><br><br>
          </div>

          
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./bootstrap/jquery-3.3.1.slim.min.js.download" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./bootstrap/popper.min.js.download"></script>
    <script src="./bootstrap/bootstrap.min.js.download"></script>

    <!-- Icons -->
    <script src="./bootstrap/feather.min.js.download"></script>
    <script>
      feather.replace()
    </script>
  

</body></html>