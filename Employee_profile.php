<?php 
  session_start();
  include_once 'backend/DB.php';

  if (isset($_SESSION['username'])) {
    $id = $_GET['empID'];

    $sql = "SELECT * 
            FROM (SELECT emp_ID,
                        position,
                        date_Hired,
                        num_Leave_Used,
                        num_Leave_Remain,
                        rate_Hour,
                        age,
                        dob,
                        gender,
                        civil_stat,
                        employee.address,
                        religion,
                        cell_Num,
                        email,
                        tel_Num,
                        CONCAT(lName,', ',fName,' ',mName) AS fullname,
                        company.name
                  FROM employee INNER JOIN company ON employee.comp_ID = company.comp_ID) AS info
            WHERE emp_ID = '$id';"; 
    $result = mysqli_query($conn, $sql);
    $rows = mysqli_fetch_assoc($result);  

  } else {
    header('location:Login_page.php?invalid-action');
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="shortcut icon" type="image/png" href="images/sad.png">

    <!-- Calendar CSS -->
    <link rel="stylesheet" href="css/calendar_style.css">

    <title>Payroll System</title>

    <!-- Bootstrap core CSS -->
    <link href="./bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./bootstrap/dashboard.css" rel="stylesheet">

    <style type="text/css">
      .tap{
        position: absolute;
        top: 95%;
      }
    </style>
 
</head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
        Payroll System
      </a>
      <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="backend/log-out.php">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">

              <li class="nav-item">
                <a class="nav-link" href="Admin_dashboard.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="Applicant_list.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                  Applicants
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link active" href="Employee_page.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  Employee
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="Client_company.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart-2"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
                  Client Company
                </a>
              </li>
              
            </ul>

          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
        
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Employee Profile</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <button class="btn btn-sm btn-outline-secondary" id="generate_Payroll" type="button" data-toggle="modal" data-target="#genPayroll">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                Generate Payroll
              </button>
            </div>
          </div>

          <!-- Modal -->
          <div class="modal fade" id="genPayroll" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Generate Payroll</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <h6>Pick Records</h6>
                  <form action="Deduct_page.php" method="POST">
                    <input type="text" class="form-control date" id="calen" name="calen" data-date-format="YYYY-MM-DD"/>
                    <br><br>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                    <input type="text" name="empid" style="display:none" <?php echo "value='".$rows['emp_ID']."'"; ?> >
                    <input type="text" name="Fname" style="display:none" <?php echo "value='".$rows['fullname']."'"; ?> >
                    <input type="text" name="position" style="display:none" <?php echo "value='".$rows['position']."'"; ?> >
                    <input type="text" name="company" style="display:none" <?php echo "value='".$rows['name']."'"; ?> >
                    <input type="text" name="rh" style="display:none" <?php echo "value='".$rows['rate_Hour']."'"; ?> >
        
                    <button type="submit" class="btn btn-primary">Proceed</button>
                  </form>

                </div>
              </div>
            </div>
          </div>

          <div class="container">

            <h4></h4><br>


          
              <div class="row justify-content-center">
                <div class="col-sm-12 ">

         
                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Employee ID #</span>
                      </div>
                      <input name="empid" type="text" style="background-color:white" aria-label="ID" class="form-control" <?php echo "value='".$rows['emp_ID']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Company: </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="civil_stat" class="form-control" <?php echo "value='".$rows['name']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Name</span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="First name" class="form-control" <?php echo "value='".$rows['fullname']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Position</span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="age" class="form-control" <?php echo "value='".$rows['position']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Age</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['age']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Date of Birth</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['dob']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Gender</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['gender']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Civil Status</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['civil_stat']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Address</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['address']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Religion</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['religion']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Cell number : </span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['cell_Num']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Tel number : </span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['tel_Num']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Email : </span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['email']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Number of leave used: </span>
                      </div>
                      <input id="leave_used" type="text" style="background-color:white" aria-label="Gender" class="form-control" <?php echo "value='".$rows['num_Leave_Used']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Number of leave remain: </span>
                      </div>
                      <input id="leave_remain" type="text" style="background-color:white" aria-label="civil_stat" class="form-control" <?php echo "value='".$rows['num_Leave_Remain']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Rate/Hour: </span>
                      </div>
                      <input id="rate_hour" type="text" style="background-color:white" aria-label="civil_stat" class="form-control" <?php echo "value='".$rows['rate_Hour']."'"; ?> readonly>
                    </div>
                    <br>

                    <br><br><br><br><br><br>

                    <button type="submit" class="btn btn-outline-dark tap offset-md-5 btn-lg" name="cancel" data-toggle="modal" data-target="#delete">Delete</button>

                </div>
              </div>
              
              <!-- Delete Employee Modal -->
              <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalCenterTitle">Notice</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <h6>Continue delete this employee?</h6>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                      <a href="backend/delete_emp.php?empid=<?php echo $rows['emp_ID']; ?>" type="submit" class="btn btn-primary">Yes</a>
                    </div>
                  </div>
                </div>
              </div>

          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./bootstrap/jquery-3.3.1.slim.min.js.download" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./bootstrap/popper.min.js.download"></script>
    <script src="./bootstrap/bootstrap.min.js.download"></script>

    <!-- Calendar scripts -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script>

    <script type="text/javascript">
      $('.date').datepicker({
        multidate: true,
         format: 'yyyy-mm-dd'
       });
    </script>


    <!-- Icons -->
    <script src="./bootstrap/feather.min.js.download"></script>
    <script>
      feather.replace()
    </script>
  

</body></html>