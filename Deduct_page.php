<?php 
  session_start();
  include_once 'backend/DB.php';

  if (isset($_SESSION['username'])) {

    $empid = $_POST['empid'];
    $Fname = $_POST['Fname'];
    $position = $_POST['position'];
    $company = $_POST['company'];
    $rh = $_POST['rh'];

    $tax =0;
    $bonus13th =0;

    $WH = 0;
    $OT = 0;
    $CA = 0;
    $netSal = 0;
    $grossSal = 0;

    $date = $_POST['calen'];
    $splitDates = explode(",", $date);
    $startDate = $splitDates[0];
    $endDate = '';
    $numberDays = 0;

    foreach ($splitDates as $value) {
      
      $sql = "SELECT total_Worked_Hours, total_OT, CA, holiday FROM employee_daily_record WHERE day='$value' AND emp_ID = '$empid' AND pr_ID = '';";
      $result = mysqli_query($conn, $sql);
      $data = mysqli_fetch_assoc($result);

      if ($data['total_Worked_Hours'] != 0) {
        $numberDays++;
      }

      $tempSal = 0;
      $WH = $WH + $data['total_Worked_Hours'];
      $OT = $OT + $data['total_OT'];
      $CA = $CA + $data['CA'];
      $tempSal = ($rh*$data['total_Worked_Hours']) + ((($rh*0.25) + $rh) * $data['total_OT']);
      
      if ($data['holiday']==25) {
        $tempSal = $tempSal + ($tempSal * 0.25);
      }elseif ($data['holiday']==30) {
        $tempSal = $tempSal + ($tempSal * 0.3);
      }

      
      $grossSal = $grossSal + $tempSal;
      $endDate = $value;
    }

    $netSal = $grossSal;

    if ($numberDays > 1 && $numberDays < 8) {
      if ($grossSal > 7691 && $grossSal < 15385) {
         $tax = 576.92 + (($grossSal-7692)*0.25);
         $netSal = $netSal - $tax;
      }elseif ($grossSal > 15384 && $grossSal < 38462) {
        $tax = 2500 + (($grossSal-15385)*0.3);
        $netSal = $netSal - $tax;
      }elseif ($grossSal > 38461 && $grossSal < 153846) {
        $tax = 9423.08 + (($grossSal-38462)*0.32);
        $netSal = $netSal - $tax;
      }elseif ($grossSal > 153845) {
        $tax = 46346.15 + (($grossSal-153846)*0.35);
        $netSal = $netSal - $tax;
      }

    }elseif ($numberDays > 7 && $numberDays < 16) {

      if ($grossSal > 16666 && $grossSal < 33333) {
        $tax = 1250 + (($grossSal-16667)*0.25);
         $netSal = $netSal - $tax;
      }elseif ($grossSal > 33332 && $grossSal < 83333) {
        $tax = 5416.67 + (($grossSal-33333)*0.3);
        $netSal = $netSal - $tax;
      }elseif ($grossSal > 83332 && $grossSal < 333333) {
        $tax = 20416.67 + (($grossSal-83333)*0.32);
        $netSal = $netSal - $tax;
      }elseif ($grossSal > 333332) {
        $tax = 100416.67 + (($grossSal-333333)*0.35);
        $netSal = $netSal - $tax;
      }

    }elseif ($numberDays > 15) {
      if ($grossSal > 33332 && $grossSal < 66667) {
        $tax = 2500 + (($grossSal-33333)*0.25);
        $netSal = $netSal - $tax;
      }elseif ($grossSal > 66666 && $grossSal < 166667) {
        $tax = 10833.33 + (($grossSal-66667)*0.3);
        $netSal = $netSal - $tax;
      }elseif ($grossSal > 166666 && $grossSal < 666667) {
        $tax = 40833.33 + (($grossSal-166667)*0.32);
        $netSal = $netSal - $tax;
      }elseif ($grossSal > 666666) {
        $tax = 200833.33 + (($grossSal-666667)*0.35);
        $netSal = $netSal - $tax;
      }

    }

    

    $sql = "SELECT MAX(benefit_ID),sss_number,sss_amount,pi_number,pi_amount,ph_amount,ph_number,emp_ID FROM benefit WHERE emp_ID='$empid';";
    $result = mysqli_query($conn, $sql);
    $data = mysqli_fetch_assoc($result);

    $netSal = $netSal - ($data['sss_amount']+$data['ph_amount']+$data['pi_amount']);

    $p_ID = $startDate.'-'.$empid;

  } else {
    header('location:Login_page.php?invalid-action');
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="shortcut icon" type="image/png" href="images/sad.png">

    <title>Payroll System</title>

    <!-- Bootstrap core CSS -->
    <link href="./bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./bootstrap/dashboard.css" rel="stylesheet">
 
</head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
        Payroll System
      </a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="backend/log-out.php">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">

              <li class="nav-item">
                <a class="nav-link" href="Admin_dashboard.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="Applicant_list.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                  Applicants
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link active" href="Employee_page.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  Employee <span class="sr-only">(current)</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="Client_company.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart-2"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
                  Client Company
                </a>
              </li>
              
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
        
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Payroll Detail</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-4">
                <button class="btn btn-sm btn-outline-secondary">Print</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
              </button>
            </div>
          </div>

          <div class="container-fluid">
            <div class="jumbotron">

              <div class="col-sm-12">
           
                  <form action="backend/generate_payroll.php" method="POST">

                  <div class="row">
                    <div class="col-sm-5 offset-sm-2 row">
                      <h6>Name : </h6><p><?php echo $Fname; ?></p>
                    </div>
                    <div class="col-sm-5 row">
                      <h6>Position : </h6><p><?php echo $position; ?></p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-5 offset-sm-2 row">
                      <h6>Employee ID# : </h6><p><?php echo $empid; ?></p>
                    </div>
                    <div class="col-sm-5 row">
                      <h6>Company : </h6><p><?php echo $company; ?></p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-5 offset-sm-2 row">
                      <h6>Total worked hour : </h6><p><?php echo $WH; ?></p>
                    </div>
                    <div class="col-sm-5 row">
                      <h6>Total OT : </h6><p><?php echo $OT; ?></p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-5 offset-sm-2 row">
                      <h6>SSS amount: </h6><p><?php echo $data['sss_amount']; ?></p>
                    </div>
                    <div class="col-sm-5 row">
                      <h6>Total CA : </h6><p><?php echo $CA; ?></p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-5 offset-sm-2 row">
                      <h6>Pag-ibig amount: </h6><p><?php echo $data['pi_amount']; ?></p>
                    </div>
                    <div class="col-sm-5 row">
                      <h6>PhilHealth amount: </h6><p><?php echo $data['ph_amount']; ?></p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-5 offset-sm-2 row">
                      <h6>Gross Salary : </h6><p><?php echo $grossSal; ?></p>
                    </div>
                    <div class="col-sm-5 row">
                      <h6>Tax : </h6><p><?php echo $tax; ?></p>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-sm-5 offset-sm-2 row">
                      <h6>Net Salary : </h6><p><?php echo $netSal; ?></p>
                    </div>
                  </div>
                  <br><br>

                  <input type="text" name="pID" style="display:none" <?php echo "value='".$p_ID."'"; ?> >
                  <input type="text" name="empID" style="display:none" <?php echo "value='".$empid."'"; ?> >
                  <input type="text" name="netSal" style="display:none" <?php echo "value='".$netSal."'"; ?> >
                  <input type="text" name="grossSal" style="display:none" <?php echo "value='".$grossSal."'"; ?> >
                  <input type="text" name="startD" style="display:none" <?php echo "value='".$startDate."'"; ?> >
                  <input type="text" name="endD" style="display:none" <?php echo "value='".$endDate."'"; ?> >
                  <input type="text" name="tax" style="display:none" <?php echo "value='".$tax."'"; ?> >
                  <input type="text" name="CA" style="display:none" <?php echo "value='".$CA."'"; ?> >
                  <input type="text" name="bonus13th" style="display:none" <?php echo "value='".$bonus13th."'"; ?> >
                  <input type="text" name="sss_a" style="display:none" <?php echo "value='".$data['sss_amount']."'"; ?> >
                  <input type="text" name="sss_id" style="display:none" <?php echo "value='".$data['sss_number']."'"; ?> >
                  <input type="text" name="pi_a" style="display:none" <?php echo "value='".$data['pi_amount']."'"; ?> >
                  <input type="text" name="pi_id" style="display:none" <?php echo "value='".$data['pi_number']."'"; ?> >
                  <input type="text" name="ph_a" style="display:none" <?php echo "value='".$data['ph_amount']."'"; ?> >
                  <input type="text" name="ph_id" style="display:none" <?php echo "value='".$data['ph_number']."'"; ?> >
                  <input type="text" name="date" style="display:none" <?php echo "value='".$date."'"; ?> >
                  <input type="text" name="numberDays" style="display:none" <?php echo "value='".$numberDays."'"; ?> >

                  <a class="btn btn-primary col-sm-2 offset-sm-1" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    Show More Details
                  </a>
                  <button type="submit" class="btn btn-primary col-sm-2 offset-sm-5">Generate Payroll</button>
                </form> 
                <br>
               
                <div class="collapse" id="collapseExample">
                  <div class="card card-body">

                    <div class="row">
                      <div class="col-sm-5 offset-sm-2 row">
                        <h6>Payroll number : </h6><p><?php echo $p_ID; ?></p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-5 offset-sm-2 row">
                        <h6>Number of worked days : </h6><p><?php echo $numberDays; ?></p>
                      </div>
                      <div class="col-sm-5 row">
                        <h6>Rate/Hour : </h6><p><?php echo $rh; ?></p>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-sm-5 offset-sm-2 row">
                        <h6>Start Date : </h6><p><?php echo $startDate; ?></p>
                      </div>
                      <div class="col-sm-5 row">
                        <h6>End Date : </h6><p><?php echo $endDate; ?></p>
                      </div>
                    </div>

                  </div>
                </div>
              </div>



            </div>
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./bootstrap/jquery-3.3.1.slim.min.js.download" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./bootstrap/popper.min.js.download"></script>
    <script src="./bootstrap/bootstrap.min.js.download"></script>
    <script src="./backend/jquery.js"></script>

    <script type="text/javascript">
      
      $(document).ready(function(){
        $('table tbody ').on('click','.btn',function(){
           var currow = $(this).closest('tr');
           var col1 = currow.find('td:eq(0)').text();
           document.getElementById("empID").value = col1;
        });
      });
    </script>

    <!-- Icons -->
    <script src="./bootstrap/feather.min.js.download"></script>
    <script>
      feather.replace()
    </script>
  

</body></html>