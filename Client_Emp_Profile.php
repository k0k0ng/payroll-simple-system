<?php 
  session_start();
  include_once 'backend/DB.php';

  if (isset($_SESSION['username'])) {
    $id = $_GET['empID'];

    $sql = "SELECT * 
            FROM (SELECT emp_ID,
                        position,
                        date_Hired,
                        num_Leave_Used,
                        num_Leave_Remain,
                        rate_Hour,
                        age,
                        dob,
                        gender,
                        civil_stat,
                        employee.address,
                        religion,
                        cell_Num,
                        email,
                        tel_Num,
                        CONCAT(lName,', ',fName,' ',mName) AS fullname,
                        lName,
                        fName,
                        mName,
                        company.name,
                        employee.comp_ID
                  FROM employee INNER JOIN company ON employee.comp_ID = company.comp_ID) AS info
            WHERE emp_ID = '$id';"; 
    $result = mysqli_query($conn, $sql);
    $rows = mysqli_fetch_assoc($result);


    $sql = "SELECT * FROM benefit WHERE emp_ID = '$id';"; 
    $result = mysqli_query($conn, $sql);
    $benefit = mysqli_fetch_assoc($result); 

  } else {
    header('location:Login_page.php?invalid-action');
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="shortcut icon" type="image/png" href="images/sad.png">

    <title>Payroll System</title>

    <!-- Bootstrap core CSS -->
    <link href="./bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./bootstrap/dashboard.css" rel="stylesheet">

    <style type="text/css">
      .tap{
        position: absolute;
        top: 95%;
      }
    </style>
 
</head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
        Payroll System
      </a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="backend/log-out.php">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">

              <li class="nav-item">
                <a class="nav-link" href="Client_dashboard.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link active" href="Client_Emp_List.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  Employee
                </a>
              </li>
              
            </ul>

          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
        
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Employee Profile</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-4">
                <button class="btn btn-sm btn-outline-secondary" style="width:80px" type="button" data-toggle="modal" data-target="#edit">Edit</button>
                <button class="btn btn-sm btn-outline-secondary" type="button" data-toggle="modal" data-target="#benefit">Benefits Info</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary" id="generate_Payroll" type="button" data-toggle="modal" data-target="#genPayroll">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                Manage DTR
              </button>
            </div>
          </div>

          <!-- Modal for Manage DTR -->
          <div class="modal fade" id="genPayroll" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Manage DTR</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="col-sm-12"> 
                    <form action="backend/Insert_dtr_record.php" method="POST">
                      <div class="input-group col-sm-10">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Enter date</span>
                        </div>
                        
                        <select name="month" class="custom-select">
                          <option value="01">Jan</option>
                          <option value="02">Feb</option>
                          <option value="03">Mar</option>
                          <option value="04">Apr</option>
                          <option value="05">May</option>
                          <option value="06">Jun</option>
                          <option value="07">Jul</option>
                          <option value="08">Aug</option>
                          <option value="09">Sep</option>
                          <option value="10">Oct</option>
                          <option value="11">Nov</option>
                          <option value="12">Dec</option>
                        </select>
                        <?php
                          $currentYear = date("Y");
                          for ($x = $currentYear; $x >= 1950; $x--){
                            $Years[] = $x;
                          }
                          echo '<select name="year" class="custom-select">';
                          foreach ($Years as $option){
                            echo '<option value="'.$option.'">'.$option.'</option>';
                          }
                          echo '</select>';
                        ?>



                      </div>
                      <br><br>

                      <div class="input-group col-sm-12">
                        <p class="col-sm-3 offset-sm-1">Regular Worked Hours</p>
                        <p class="col-sm-2">Total OT</p>
                        <p class="col-sm-2">Total UT</p>
                        <p class="col-sm-2">Cash Advance</p>
                        <p class="col-sm-2">Holiday</p>
                      </div>
                      
                        <?php
                          for ($x = 1; $x <= 31; $x++){
                            $Days[] = $x;
                          }

                          foreach ($Days as $option){
                            echo '<div class="input-group col-sm-12">';

                            echo '<input name="empID" type="text" style="display:none"  class="form-control" value="'.$rows['emp_ID'].'" >';
                            echo "<input name='day".$option."' type='text' style='display:none' value='".$option."'>";
                            echo '<div class="input-group col-sm-4">';
                            echo '<div class="input-group-prepend">';
                            echo '<span class="input-group-text">Day '.$option.': </span>';
                            echo "</div>";
                            echo '<input name="wh'.$option.'" type="text" style="background-color:white" aria-label="ID" class="form-control" >';
                            echo "</div>";

                            echo '<div class="input-group col-sm-2">';
                            echo '<input name="ot'.$option.'" type="text"  style="background-color:white" aria-label="ID" class="form-control" >';
                            echo "</div> ";

                            echo '<div class="input-group col-sm-2">';
                            echo '<input name="ut'.$option.'" type="text"  style="background-color:white" aria-label="ID" class="form-control" >';
                            echo "</div> ";

                            echo '<div class="input-group col-sm-2">';
                            echo '<input name="ca'.$option.'" type="text"  style="background-color:white" aria-label="ID" class="form-control" >';
                            echo "</div> ";

                            echo '<div class="input-group col-sm-2">';                              
                            echo '<select name="h'.$option.'" class="custom-select" id="inputGroupSelect01">';
                            echo '<option selected>Choose...</option>';
                            echo '<option value="25">25%</option>';
                            echo '<option value="30">30%</option>';
                            echo '</select>';
                            echo "</div> ";
                            echo "</div> <br>";
                          }
                        ?>
                      

                      <br><br><br>
                      <button type="submit" class="btn btn-primary">Submit Record</button>
                    </form>
                  </div> <br>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal for Manage Employee Benefits -->
          <div class="modal fade" id="benefit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Edit Employee Profile</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="col-sm-12"> 

                    <form action="backend/update_emp_benefit.php" method="POST">

                      <input type="text" name="empid" style="display:none" <?php echo "value='".$rows['emp_ID']."'"; ?> >

                        <div class="input-group col-sm-7 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">SSS number: </span>
                          </div>
                          <input type="text" style="background-color:white" aria-label="age" name="sss_number" class="form-control" <?php echo "value='".$benefit['sss_number']."'"; ?> >
                        </div>
                        <br>

                        <div class="input-group col-sm-7 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">SSS amount: </span>
                          </div>
                          <input type="text" style="background-color:white" name="sss_amount" class="form-control" <?php echo "value='".$benefit['sss_amount']."'"; ?> >
                        </div>
                        <br>

                        <div class="input-group col-sm-7 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">PhilHealth number: </span>
                          </div>
                          <input type="text" style="background-color:white" name="ph_number" class="form-control" <?php echo "value='".$benefit['ph_number']."'"; ?> >
                        </div><br>

                        <div class="input-group col-sm-7 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">PhilHealth amount: </span>
                          </div>
                          <input type="text" style="background-color:white" name="ph_amount" class="form-control" <?php echo "value='".$benefit['ph_amount']."'"; ?> >
                        </div><br>


                        <div class="input-group col-sm-7 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Pag-ibig number: </span>
                          </div>
                          <input type="text" style="background-color:white" name="pi_number" class="form-control" <?php echo "value='".$benefit['pi_number']."'"; ?> >
                        </div><br>

                        <div class="input-group col-sm-7 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Pag-ibig amount: </span>
                          </div>
                          <input type="text" style="background-color:white" name="pi_amount" class="form-control" <?php echo "value='".$benefit['pi_amount']."'"; ?> >
                        </div><br>
                   
                    <br><br><br>
                    <button type="submit" class="btn btn-primary offset-sm-9">Save Changes</button>
                    </form>
                    <br><br>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal for Edit Employee Profile -->
          <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Edit Employee Profile</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="col-sm-12"> 

                    <form action="backend/update_emp_profile.php" method="POST">

                      <input type="text" name="empid" style="display:none" <?php echo "value='".$rows['emp_ID']."'"; ?> >
                      <input type="text" name="compid" style="display:none" <?php echo "value='".$rows['comp_ID']."'"; ?> >

                      <div class="input-group col-sm-12">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Name : </span>
                        </div>
                        <input type="text" style="background-color:white" aria-label="First name" name="fname" class="form-control" <?php echo "value='".$rows['fName']."'"; ?>>
                        <input type="text" style="background-color:white" aria-label="Last name" name="lname" class="form-control" <?php echo "value='".$rows['lName']."'"; ?> >
                        <input type="text" style="background-color:white" aria-label="Middel name" name="mname" class="form-control" <?php echo "value='".$rows['mName']."'"; ?> >
                      </div>
                      <br>

                      <div class="row">
                        <div class="input-group col-sm-5 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Position</span>
                          </div>
                          <input type="text" style="background-color:white" aria-label="age" name="position" class="form-control" <?php echo "value='".$rows['position']."'"; ?> >
                        </div>
                        <div class="input-group col-sm-5">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Gender</span>
                          </div>
                          <input type="text" style="background-color:white" name="gender" class="form-control" <?php echo "value='".$rows['gender']."'"; ?> >
                        </div>
                      </div><br>
                   
                      <div class="row">
                        <div class="input-group col-sm-5 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Age</span>
                          </div>
                          <input type="text" style="background-color:white" name="age" class="form-control" <?php echo "value='".$rows['age']."'"; ?> >
                        </div>
                        <br>
                        <div class="input-group col-sm-5">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Date of Birth</span>
                          </div>
                          <input type="text" style="background-color:white" name="dob" class="form-control" <?php echo "value='".$rows['dob']."'"; ?> >
                        </div>
                      </div><br>

                      <div class="input-group col-sm-10 offset-sm-1">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Address</span>
                        </div>
                        <input type="text" style="background-color:white" name="address" class="form-control" <?php echo "value='".$rows['address']."'"; ?> >
                      </div>
                      <br>

                      <div class="row">
                        <div class="input-group col-sm-5 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Civil Status</span>
                          </div>
                          <input type="text" style="background-color:white" name="civil_stat" class="form-control" <?php echo "value='".$rows['civil_stat']."'"; ?> >
                        </div>
                        <br>
                        <div class="input-group col-sm-5">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Religion</span>
                          </div>
                          <input type="text" style="background-color:white" name="religion" class="form-control" <?php echo "value='".$rows['religion']."'"; ?> >
                        </div>
                      </div><br>

                      <div class="row">
                        <div class="input-group col-sm-6 ">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Cell number : </span>
                          </div>
                          <input type="text" style="background-color:white" name="cell_num" class="form-control" <?php echo "value='".$rows['cell_Num']."'"; ?> >
                        </div>
                        <br>
                        <div class="input-group col-sm-6">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Tel number : </span>
                          </div>
                          <input type="text" style="background-color:white" name="tel_num" class="form-control" <?php echo "value='".$rows['tel_Num']."'"; ?> >
                        </div>
                      </div><br>

                      <div class="input-group col-sm-10 offset-sm-1">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Email : </span>
                        </div>
                        <input type="text" style="background-color:white" name="email" class="form-control" <?php echo "value='".$rows['email']."'"; ?> >
                      </div>
                      <br>

                      <div class="row">
                        <div class="input-group col-sm-5 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Number of leave used: </span>
                          </div>
                          <input type="text" style="background-color:white" name="lu" aria-label="Gender" class="form-control" <?php echo "value='".$rows['num_Leave_Used']."'"; ?> >
                        </div>
                        <br>
                        <div class="input-group col-sm-5">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Number of leave remain: </span>
                          </div>
                          <input type="text" style="background-color:white" name="lr" aria-label="civil_stat" class="form-control" <?php echo "value='".$rows['num_Leave_Remain']."'"; ?> >
                        </div>
                      </div><br>

                      <div class="input-group col-sm-5 offset-sm-1">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Rate/Hour: </span>
                        </div>
                        <input type="text" style="background-color:white" name="rh" aria-label="civil_stat" class="form-control" <?php echo "value='".$rows['rate_Hour']."'"; ?> >
                      </div>
                 
                    <br><br><br>
                    <button type="submit" class="btn btn-primary offset-sm-9">Save Changes</button>
                    </form>
                    <br><br>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="container">

            <h4></h4><br>

          
              <div class="row justify-content-center">
                <div class="col-sm-12 ">
                  <!-- <img src="./images/Profile.jpg" alt="./images/Profile.jpg" class="rounded-circle" width="85" height="75">
                  -->
                  <form action="hire_process.php" method="POST">
                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Employee ID #</span>
                      </div>
                      <input name="App_ID" type="text" style="background-color:white" aria-label="ID" class="form-control" <?php echo "value='".$rows['emp_ID']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Company: </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="civil_stat" class="form-control" <?php echo "value='".$rows['name']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Name</span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="First name" class="form-control" <?php echo "value='".$rows['fullname']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Position</span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="age" class="form-control" <?php echo "value='".$rows['position']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Age</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['age']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Date of Birth</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['dob']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Gender</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['gender']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Civil Status</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['civil_stat']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Address</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['address']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Religion</span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['religion']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Cell number : </span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['cell_Num']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Tel number : </span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['tel_Num']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Email : </span>
                      </div>
                      <input type="text" style="background-color:white" class="form-control" <?php echo "value='".$rows['email']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Number of leave used: </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="Gender" class="form-control" <?php echo "value='".$rows['num_Leave_Used']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Number of leave remain: </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="civil_stat" class="form-control" <?php echo "value='".$rows['num_Leave_Remain']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Rate/Hour: </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="civil_stat" class="form-control" <?php echo "value='".$rows['rate_Hour']."'"; ?> readonly>
                    </div>
                    <br><br><br><br><br><br><br>
                  </form>

                  <form action="Client_Emp_List.php" method="POST">
                    <button type="submit" class="btn btn-outline-dark tap offset-md-5 btn-lg" name="cancel">Back</button>
                  </form>


                </div>
              </div>
              <br><br><br><br><br><br>
          </div>

          
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./bootstrap/jquery-3.3.1.slim.min.js.download" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./bootstrap/popper.min.js.download"></script>
    <script src="./bootstrap/bootstrap.min.js.download"></script>

    <!-- Icons -->
    <script src="./bootstrap/feather.min.js.download"></script>
    <script>
      feather.replace()
    </script>
  

</body></html>