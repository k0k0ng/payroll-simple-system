<?php 
	session_start();
	include_once 'backend/DB.php';

	if (isset($_SESSION['username'])) {
		$username = $_SESSION['username'];
		$pass = $_SESSION['pword'];
		$sql = "SELECT type FROM accounts WHERE username='$username' AND pword='$pass';"; 
		$result = mysqli_query($conn, $sql);
		$resultCheck = mysqli_num_rows($result);
		$rows = mysqli_fetch_assoc($result);
		
		if ($rows['type']==='client') {
			header('location:Client_dashboard.php');
		} elseif ($rows['type']==='admin') {
			header('location:Admin_dashboard.php');
		}	
	}

 ?>

<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="shortcut icon" type="image/png" href="images/sad.png">

    <!-- Bootstrap core CSS -->
    <link href="./bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./bootstrap/dashboard.css" rel="stylesheet">

	<title>Log In</title>

	<style type="text/css">
		
		body{
			text-align: center;
		}
		.main{
			position: absolute;
			top: 50px;
			left: 36%;
			width: 25%;
			height: 70%;
			font-family: courier new;
			text-align: center;
			border-radius: 5px;
		}

		.inp{
			position: relative;
			top: 40px;
			width: 300px;
			height: 40px;
			border-radius: 5px;
			padding-left: 10px;
		}

		.butt{
			position: relative;
			top: 40px;
			width: 200px;
			height: 40px;
			border-radius: 15px;
			cursor: pointer;
		}
	</style>

</head>
<body>
	<div class="container">
	<div class="main col col-lg-3 shadow-lg p-3 mb-5 bg-white rounded">
		<form action="backend/Check.php" method="POST">
			<br><br><h2>Payroll System</h2>
			<input type="text" name="username" placeholder="Username" class="inp form-control col col-lg-12 shadow p-3 mb-5 bg-white rounded" required>
			
			<input type="password" name="pword" placeholder="Password" class="inp form-control col col-lg-12 shadow p-3 mb-5 bg-white rounded" required><br><br><br>

			<button type="submit" name="login" class="butt btn btn-primary col col-lg-12">Log in</button>
			
	</div>
	</div>


	<script src="./bootstrap/jquery-3.3.1.slim.min.js.download" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./bootstrap/popper.min.js.download"></script>
    <script src="./bootstrap/bootstrap.min.js.download"></script>

</body>
</html>