<?php 
	session_start();

	unset($_SESSION['username']);
	unset($_SESSION['pword']);
	unset($_SESSION['compID']);
	session_destroy();
	header('location:../Login_page.php');