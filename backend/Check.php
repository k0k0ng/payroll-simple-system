<?php 
	
	session_start();
	include_once 'DB.php';

	$username = mysqli_real_escape_string($conn, $_POST['username']);
	$password = mysqli_real_escape_string($conn, $_POST['pword']);

	$sql = "SELECT * FROM accounts WHERE username='$username';"; 
	$result = mysqli_query($conn, $sql);
	$resultCheck = mysqli_num_rows($result);
	$rows = mysqli_fetch_assoc($result);

	if ($password === $rows['pword'] && $rows['type'] === 'admin') {
		
		$_SESSION['username'] = $username;
		$_SESSION['pword'] = $password;
		header('location:../Admin_dashboard.php?login=successful');

	} elseif ($password === $rows['pword'] && $rows['type'] === 'staff') {
		
		$_SESSION['username'] = $username;
		$_SESSION['pword'] = $password;
		$_SESSION['compID'] = $rows['comp_ID'];
		header('location:../Client_dashboard.php?login=successful');

	} else {
		header('location: ../Login_page.php?login=error');
	}



