<?php 
  session_start();
  include_once 'backend/DB.php';

  if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
    $sql = "SELECT * FROM accounts WHERE username='$username';"; 
    $result = mysqli_query($conn, $sql);
    $resultCheck = mysqli_num_rows($result);
    $rows = mysqli_fetch_assoc($result);  
  } else {
    header('location:Login_page.php?invalid-action');
  }
?>

<!DOCTYPE html>

<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="shortcut icon" type="image/png" href="images/sad.png">

    <title>Payroll System</title>

    <!-- Bootstrap core CSS -->
    <link href="./bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./bootstrap/dashboard.css" rel="stylesheet">
 
</head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
        Payroll System
      </a>
      <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="backend/log-out.php">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">

              <li class="nav-item">
                <a class="nav-link" href="Client_dashboard.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="Client_Emp_List.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  Employee <span class="sr-only">(current)</span>
                </a>
              </li>

            
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
        
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Manage Employee DTR</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-4">
                <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
              </button>
            </div>
          </div>

          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Employee ID</th>
                  <th>Name</th>
                  <th>Gender</th>
                  <th>Position</th>
                  <th>Compnany</th>
                </tr>
              </thead>
              <tbody>

                <?php 
                  $get = "SELECT CONCAT(applicants.lname,', ',applicants.fname,' ',applicants.mname) AS Ename,
                                  applicants.gender,
                                  employee.emp_ID,
                                  employee.position,
                                  company.name
                          FROM employee 
                          INNER JOIN applicants ON employee.app_ID = applicants.app_ID 
                          INNER JOIN company ON employee.comp_ID = company.comp_ID;";

                  $data = mysqli_query($conn, $get);
                  $resultCheck = mysqli_num_rows($data);

                  if ($resultCheck > 0) {
                    while ( $rows = mysqli_fetch_assoc($data)) {
                       echo "<form action='Applicant_info.php' method='POST'>
                               <tr>
                               <td><div name='id'>".$rows['emp_ID']."</div></td>
                               <td>".$rows['Ename']."</td>
                               <td>".$rows['gender']."</td>
                               <td>".$rows['position']."</td>
                               <td>".$rows['name']."</td>
                               <td>
                               <input type='text' style='display:none' id='appID' name='AppID'/>
                               <button class='btn btn-sm btn-outline-secondary offset-md-6'>View</button></td>
                             </tr></form>";
                    }
                    echo "</table>";
                  }else{

                  }

                 ?>
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./bootstrap/jquery-3.3.1.slim.min.js.download" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./bootstrap/popper.min.js.download"></script>
    <script src="./bootstrap/bootstrap.min.js.download"></script>
    <script src="./backend/jquery.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        $('table tbody tr').click(function(){
          $(this).html(function(){
            alert($('td:first').text());
          }));
        });
      });
    </script>

    <!-- Icons -->
    <script src="./bootstrap/feather.min.js.download"></script>
    <script>
      feather.replace()
    </script>
  

</body></html>