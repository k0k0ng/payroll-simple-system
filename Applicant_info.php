<?php 
  session_start();
  include_once 'backend/DB.php';

  if (isset($_SESSION['username'])) {
    $id = $_GET['appID'];
    $sql = "SELECT * FROM applicant WHERE app_ID='$id';"; 
    $result = mysqli_query($conn, $sql);
    $resultCheck = mysqli_num_rows($result);
    $rows = mysqli_fetch_assoc($result);  

    $sad = $rows['app_ID'];
  } else {
    header('location:Login_page.php?invalid-action');
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="shortcut icon" type="image/png" href="images/sad.png">

    <title>Payroll System</title>

    <!-- Bootstrap core CSS -->
    <link href="./bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./bootstrap/dashboard.css" rel="stylesheet">

    <style type="text/css">
      .tap{
        position: absolute;
        top: 95%;
      }
    </style>
 
</head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
        Payroll System
      </a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="backend/log-out.php">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">

              <li class="nav-item">
                <a class="nav-link" href="Admin_dashboard.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link active" href="Applicant_list.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                  Applicants
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="Employee_page.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  Employee
                </a>
              </li>

              

              <li class="nav-item">
                <a class="nav-link" href="Client_company.php">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart-2"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
                  Client Company
                </a>
              </li>
              
            </ul>

          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
        
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Applicant Information</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-4">
                <button class="btn btn-sm btn-outline-secondary" style="width:80px" type="button" data-toggle="modal" data-target="#edit">Edit</button>
              </div>
            </div>
          </div>

          <div class="container">

            <!-- Modal for Edit Applicant Profile -->
          <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Edit Applicant Profile</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="col-sm-12"> 

                    <form action="backend/update_app_profile.php" method="POST">

                      <input type="text" name="appid" style="display:none" <?php echo "value='".$rows['app_ID']."'"; ?> >

                        <div class="input-group col-sm-10 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Name: </span>
                          </div>
                          <input type="text"  name="lname" class="form-control" <?php echo "value='".$rows['lName']."'"; ?> >
                          <input type="text"  name="fname" class="form-control" <?php echo "value='".$rows['fName']."'"; ?> >
                          <input type="text"  name="mname" class="form-control" <?php echo "value='".$rows['mName']."'"; ?> >
                        </div>
                        <br>

                        <div class="input-group col-sm-5 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Date of birth: </span>
                          </div>
                          <input type="text" style="text-align:center" name="dob" class="form-control" <?php echo "value='".$rows['dob']."'"; ?> >
                        </div><br>

                        <div class="input-group col-sm-4 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Gender: </span>
                          </div>
                          <input type="text" style="text-align:center" name="gender" class="form-control" <?php echo "value='".$rows['gender']."'"; ?> >
                        </div><br>


                        <div class="input-group col-sm-4 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Civil status: </span>
                          </div>
                          <input type="text" style="text-align:center" name="civil_stat" class="form-control" <?php echo "value='".$rows['civil_stat']."'"; ?> >
                        </div><br>

                        <div class="input-group col-sm-7 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Address: </span>
                          </div>
                          <input type="text" style="background-color:white" name="address" class="form-control" <?php echo "value='".$rows['address']."'"; ?> >
                        </div><br>

                        <div class="input-group col-sm-5 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Religion: </span>
                          </div>
                          <input type="text" style="text-align:center" name="religion" class="form-control" <?php echo "value='".$rows['religion']."'"; ?> >
                        </div><br>

                        <div class="input-group col-sm-6 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Email: </span>
                          </div>
                          <input type="text" style="background-color:white" name="email" class="form-control" <?php echo "value='".$rows['email']."'"; ?> >
                        </div><br>

                        <div class="input-group col-sm-6 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Cell number: </span>
                          </div>
                          <input type="text" style="background-color:white" name="cell_num" class="form-control" <?php echo "value='".$rows['cell_Num']."'"; ?> >
                        </div><br>

                        <div class="input-group col-sm-6 offset-sm-1">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Tel number: </span>
                          </div>
                          <input type="text" style="background-color:white" name="tel_num" class="form-control" <?php echo "value='".$rows['tel_Num']."'"; ?> >
                        </div><br>
                   
                    <br><br><br>
                    <button type="submit" class="btn btn-primary offset-sm-9">Save Changes</button>
                    </form>
                    <br><br>
                  </div>
                </div>
              </div>
            </div>
          </div>

            <h4></h4><br>

          
              <div class="row justify-content-center">
                <div class="col-sm-12 ">
                  <!-- <img src="./images/Profile.jpg" alt="./images/Profile.jpg" class="rounded-circle" width="85" height="75">
                  -->
                  <form action="hire_process.php" method="POST">
                    <div class="input-group col-sm-4 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Applicant ID : </span>
                      </div>
                      <input name="App_ID" type="text" style="background-color:white" aria-label="ID" class="form-control" <?php echo "value='".$rows['app_ID']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-8 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Name : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="First name" class="form-control" <?php echo "value='".$rows['fName']."'"; ?> readonly>
                      <input type="text" style="background-color:white" aria-label="Last name" class="form-control" <?php echo "value='".$rows['lName']."'"; ?> readonly>
                      <input type="text" style="background-color:white" aria-label="Middel name" class="form-control" <?php echo "value='".$rows['mName']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-3 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Age : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="age" class="form-control" <?php echo "value='".$rows['age']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-3 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">DOB : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="DOB" class="form-control" <?php echo "value='".$rows['dob']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-3 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Gender : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="Gender" class="form-control" <?php echo "value='".$rows['gender']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-3 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Civil Status : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="civil_stat" class="form-control" <?php echo "value='".$rows['civil_stat']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-7 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Address : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="street" class="form-control" <?php echo "value='".$rows['address']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-3 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Religion : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="religion" class="form-control" <?php echo "value='".$rows['religion']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-4 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Email : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="email" class="form-control" <?php echo "value='".$rows['email']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-4 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Tel number : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="tel_num" class="form-control" <?php echo "value='".$rows['tel_Num']."'"; ?> readonly>
                    </div>
                    <br>

                    <div class="input-group col-sm-4 offset-sm-1">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Cell number : </span>
                      </div>
                      <input type="text" style="background-color:white" aria-label="cell_num" class="form-control" <?php echo "value='".$rows['cell_Num']."'"; ?> readonly>
                    </div>
                    <br><br><br><br><br><br><br><br><br>

                  
                    <button type="submit" class="btn btn-outline-primary tap offset-md-3 btn-lg" name="hire">Hire Applicant</button>
                  </form>
                  
                  <button type="submit" class="btn btn-outline-dark tap offset-md-6 btn-lg" name="cancel" data-toggle="modal" data-target="#delete">Reject Applicant</button>
            
                </div>
              </div>
              <br><br><br><br><br><br><br><br>
          </div>
        </main>
      </div>

      <!-- Delete Applicant Modal -->
      <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Notice</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h6>Continue delete this applicant?</h6>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              <a href="backend/Delete.php?appID=<?php echo $rows['app_ID']; ?>" type="submit" class="btn btn-primary">Yes</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./bootstrap/jquery-3.3.1.slim.min.js.download" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./bootstrap/popper.min.js.download"></script>
    <script src="./bootstrap/bootstrap.min.js.download"></script>


    <script type="text/javascript">

      function getID() {

        <?php  ?>
      }

    </script>

    <!-- Icons -->
    <script src="./bootstrap/feather.min.js.download"></script>
    <script>
      feather.replace()
    </script>
  

</body></html>